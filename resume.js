/**
 * Resume
 *
 * Thank you for checking out my code. For this, I have used ES5 for maximum
 * compatibility. My fav tech is ESNext, Typescript JSDoc and SvelteKit.
 *
 * https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html
 * https://kit.svelte.dev/
 *
 * Please feel free to reach out to discuss JavaScript and other technologies
 * more.
 *
 * Copyright Jack Farley 2021
 */
var resume = {
  "profile": "A computer engineer with software in the brain and over 10 years of experience developing software. Specialised in proactive problem solving, and engineering complete and flexible solutions. A passion for coding elegant, responsive web applications for data-rich environments. Looking to contribute towards the understanding of science and the environment by using engineering knowledge and skills to help explore and present new science.",
  "favouredTech": [ "JavaScript", "Node.js", "Svelte", "HTML", "Japa", "WebDriverIO", "Git", "Vim", "Linux", "uPlot", "Leaflet.js", "QGIS", "KiCAD" ],
  "experiences": [
    {
      "company": {
        "name": "Department of Conservation (DOC)",
        "href": "http://www.doc.govt.nz"
      },
      "title": "Contract Electronics and Software Engineer",
      "location": {
        "country": {
          "code": "nz",
          "name": "New Zealand"
        },
        "name": "Wellington, New Zealand"
      },
      "startDate": {
        "month": 02,
        "year": 2022
      },
      "description": [
        "Enhanced Kakapo Project Windows applications for bird monitoring",
        "Prototyped NB-IoT and Satellite communication microcontroller projects",
        "Wrote Windows-based development tools for developing new transmitter hardware"
      ],
      "technologies": [
        "C#", "AWS S3", "C", "PIC microcontrollers", "Iridium SBD", "NB-IoT", "LTE-M", "MQTT",
        "QGIS"
      ],
      "tags": [ "software", "engineering", "hardware" ]
    },
    {
      "company": {
        "name": "Upper Hutt Community Rescue (UHCR) (NZRT8)",
        "href": "https://uhcr.org.nz"
      },
      "title": "Executive and Senior Operational Team Member",
      "startDate": {
        "year": 2022,
        "month": 1
      },
      "description": [
        "Swiftwater rescue technician",
        "Rope responder",
        "First aider",
        "4WD vehicle operator",
        "Technology expert"
      ],
      "tags": [ "rescue", "safety" ]
    },
    {
      "company": {
        "name": "National Oceanography Centre (NOC)",
        "href": "http://www.noc.ac.uk"
      },
      "location": {
        "country": {
          "code": "uk",
          "name": "United Kingdom"
        },
        "name": "Southampton, United Kingdom"
      },
      "startDate": {
        "month": 02,
        "year": 2018
      },
      "endDate": {
        "month": 06,
        "year": 2021
      },
      "description": [
        "Developed the <a href=\"https://noc.ac.uk/projects/oceanids\" target=\"_blank\">Oceanids</a> Command and Control (C2) web application to monitor and control autonomous marine vehicles",
        "Led software development iterations",
        {
          "description": "Assisted with the design, documentation and implementation of Kubernetes-based infrastructure-as-code (IaC) infrastructure"
        },
        {
          "description": "Provided level 3 support for the <a href=\"https://noc.ac.uk/projects/oceanids\" target=\"_blank\">Oceanids</a> C2 infrastructure and application",
          "className": "noprint"
        },
        {
          "description": "Piloted autonomous vehicles, including Slocums, Seagliders and NOC Autosubs",
          "className": "noprint"
        },
        {
          "description": "Assisted in the deployment and maintenance of autonomous vehicles during field trials",
          "className": "noprint"
        },
        {
          "description": "Jointly published the <a href=\"https://noc.ac.uk/publication/526132\" target=\"_blank\">\"Marine Science from an Armchair: A Unified Piloting Framework for Autonomous Marine Vehicles\" paper</a> for the 2019 Marseille IEEE Oceans conference",
          "className": "noprint"
        },
        "Educated the public at NOC open days"
      ],
      "title": "Software Engineer",
      "technologies": [ "Nuxt", "Python", "PostgreSQL", "HTML", "JavaScript",
          "CSS", "Leaflet.js", "uPlot", "Plotly.js", "Node.js", "Gitlab", "Git", "KiCAD" ],
      "tags": [ "web", "software", "engineering", "ocean" ]
    },
    {
      "company": {
        "name": "OpenSeaLab Hackathon",
        "href": "https://www.emodnet.eu/en/opensealab"
      },
      "location": {
        "country": {
          "code": "be",
          "name": "Belgium"
        },
        "name": "Ghent, Belgium"
      },
      "date": {
        "month": 09,
        "year": 2019
      },
      "description": [
        "Designed and developed the <a href=\"https://gitlab.com/bytesnz/byte-bear\">Byte Bear</a> concept"
      ],
      "title": "Participant / Runner-up",
      "tags": [ "web", "software", "ocean" ],
      "technologies": [ "Preact", "Plotly.js" ],
      "className": "noprint",
    },
    {
      "company": {
        "name": "Reef Doctor",
        "href": "http://www.reefdoctor.org"
      },
      "location": {
        "country": {
          "code": "mg",
          "name": "Madagascar"
        },
        "name": "Ifaty, Madagascar"
      },
      "startDate": {
        "month": 06,
        "year": 2017
      },
      "endDate": {
        "month": 12,
        "year": 2017
      },
      "description": [
        "Developed a database and web application for storing and analysing Reef Doctor fisheries and environmental data",
        "Created scripts to clean, correlate, import, and summarise fisheries catch and effort data so that it could be used in reports to the Malagasy government",
        "Reengineered the Reef Doctor local network to include a central data server to empower the research and analysis carried out by Reef Doctor",
        {
          "description": "Provided technical support for volunteers and staff",
          "className": "noprint"
        },
        {
          "description": "Carried out fish, invertebrate, benthic, and seagrass underwater surveys",
          "className": "noprint",
        },
        {
          "description": "Carried out coral transplantation and monitoring, and artificial reef building projects",
          "className": "noprint",
        },
        {
          "description": "Ensured dive operations kept running by carrying out maintenance on equipment, including dive computers, maintenance of scientific sensors and assisting in cylinder maintenance and inspections",
          "className": "noprint",
        },
        {
          "description": "Qualified as a PADI Divemaster",
          "className": "noprint",
        }
      ],
      "title": "Scientific Research Assistant and Divemaster Intern",
      "technologies": [ "React", "Redux", "MongoDB", "HTML", "JavaScript",
          "CSS", "Leaflet.js", "Plotly.js", "Node.js", "C", "Git" ],
      "tags": [ "web", "software", "engineering", "ocean" ]
    },
    {
      "company": {
        "name": "Scoota",
        "href": "http://www.scoota.com"
      },
      "isCurrent": true,
      "location": {
        "country": {
          "code": "gb",
          "name": "United Kingdom"
        },
        "name": "London, United Kingdom"
      },
      "startDate": {
        "month": 12,
        "year": 2016
      },
      "endDate": {
        "month": 06,
        "year": 2017
      },
      "description": [
        "Developed their new ad management platform using Angular so their clients could manage their ad campaigns and track their impact at a click of a button",
        "Implemented a new look for their Ad creatives editor using AngularJS to improve usability and functionality",
        {
          "description": "Developed ad formats publishing using Amazon Lambda, triggered by Amazon SNS to drastically reduce the processing time of an ad",
          "className": "noprint",
        },
        "Contributed towards the ongoing development of their Django backend so that it met the needs of the management platform"
      ],
      "title": "JavaScript Software Developer",
      "technologies": [ "HTML", "JavaScript", "CSS", "Node.js",
          "Amazon Lambda", "Amazon SNS", "Jenkins", "Angular", "AngularJS",
          "Mocha", "Bootstrap", "Python", "Django", "JSONRPC", "ChartJS", "Git", "Jira" ],
      "tags": [ "web", "software", "engineering" ]
    },
    {
      "company": {
        "id": 10077470,
        "name": "NHS Wales Informatics Service",
        "href": "http://www.wales.nhs.uk/nwis/"
      },
      "id": 747076814,
      "isCurrent": true,
      "location": {
        "country": {
          "code": "gb",
          "name": "United Kingdom"
        },
        "name": "Cardiff, United Kingdom"
      },
      "startDate": {
        "month": 12,
        "year": 2015
      },
      "endDate": {
        "month": 12,
        "year": 2016
      },
      "description": [
        "Developed electronic forms using Orbeon Forms (XForms, XSLT, XML), HTML, CSS, JavaScript, Java and Node.js so patient reported experience and outcome measures could captured to ensure patients received the best care for them",
        "Led the development of some forms, working closely with architects and clinicians to analyse requirements and give advice on system capabilities",
        "Mentored other members of the team during development to ensure forms were developed on time and to a high standard",
        {
          "description": "Championed the use of the Git versioning system and TFS within the team to increase the efficiency of the team",
          "className": "noprint",
        },
        {
          "description": "Lead efficient agile development by designing for code reuse",
          "className": "noprint",
        }
      ],
      "title": "Senior Software Developer",
      "technologies": [ "HTML", "JavaScript", "CSS", "Node.js",
          "Orbeon Forms",  "XForms", "XSLT", "XML", "Git", "TFS", "Gulp", "ColdFusion" ],
      "tags": [ "web", "software", "engineering" ]
    },
    {
      "startDate": {
        "month": 7,
        "year": 2014
      },
      "endDate": {
        "month": 10,
        "year": 2014
      },
      "company": {
        "name": "NGO Taxi",
        "href": "http://ngotaxi.org"
      },
      "location": {
        "name": "Cuzco, Peru"
      },
      "description": [
        "Started implementing a booking system for NGO Taxi to use in obtaining and managing their volunteers",
        {
          "description": "Added alerts and processing prompt scripts to NGO Taxi's Google Sheets so they could keep better track of volunteer processing deadlines",
          "className": "noprint",
        }
      ],
      "title": "Volunteer",
      "technologies": [ "HTML", "CSS", "JavaScript", "Wordpress", "PHP", "Drupal" ],
      "tags": [ "web", "software", "engineering" ],
      "className": "noprint",
    },
    {
      "startDate": {
        "month": 4,
        "year": 2014
      },
      "endDate": {
        "month": 11,
        "year": 2015
      },
      "description": "Overseas Experience (OE) travelling in South and North America"
    },
    {
      "company": {
        "name": "New Zealand Government",
        "href": "https://www.govt.nz/"
      },
      "endDate": {
        "month": 4,
        "year": 2014
      },
      "id": 729440249,
      "isCurrent": false,
      "location": {
        "name": "Wellington, New Zealand"
      },
      "startDate": {
        "month": 9,
        "year": 2010
      },
      "description": [
        "Improved the capability of teams by researching and developing complex computer systems to meet stringent requirements and specifications",
        "Increased the efficiency and resilience to services by developing programs and scripts in Perl, Bash and C to process data and monitor processing systems",
        "Contributed to web applications for monitoring of processes and systems",
        "Designed, documented and implemented Linux-heavy networks",
        {
          "description": "Decreased deployment times by designing and implementing automated Linux operating system (OS) building and virtualisation using kernel virtual machine (KVM)",
          "className": "noprint",
        },
        {
          "description": "Increased the security of internal computer networks using OS hardening and, network and firewall design",
          "className": "noprint",
        },
        {
          "description": "Provided level 3 technical support for data processing systems and internal computer networks",
          "className": "noprint",
        },
        {
          "description": "Knowledge expert on a number of data processing systems and virtual machine infrastructure",
          "className": "noprint",
        },
        "Mentored and taught colleagues on computer systems and the Linux OS",
        {
          "description": "Purchased required hardware through internal procurement system dealing directly with suppliers",
          "className": "noprint"
        }
      ],
      "title": "Computer Systems Engineer",
      "technologies": [ "Linux", "Perl", "VMware", "Cisco Networking Equipment", "HTML", "JavaScript" ],
      "tags": [ "web", "software", "hardware", "engineering" ]
    },
    {
      "company": {
        "name": "New Zealand Government",
        "href": "https://www.govt.nz/"
      },
      "endDate": {
        "month": 9,
        "year": 2010
      },
      "id": 729438306,
      "isCurrent": false,
      "location": {
        "name": "Wellington, New Zealand"
      },
      "startDate": {
        "month": 1,
        "year": 2010
      },
      "description": [
        "Designed and implemented a Perl-based web interface for managing customer job requests",
        "Administered three computer networks of around 50 computers total, which included workstations and high availability servers",
        {
          "description": "Managed network and virtual infrastructures",
          "className": "noprint",
        },
        {
          "description": "Implemented network monitoring through Nagios, SAN storage, Puppet configuration management and server viritualisation using VMware ESX and vSphere",
          "className": "noprint",
        }
      ],
      "title": "Computer Systems Administrator",
      "technologies": [ "Linux (Red Hat compatible)", "Perl", "VMware",
          "Cisco Networking Equipment", "EMC SANs", "Puppet", "HTML", "CSS",
          "JavaScript" ],
      "tags": [ "web", "software", "engineering" ]
    },
    {
      "company": {
        "name": "New Zealand Government",
        "href": "https://www.govt.nz/"
      },
      "endDate": {
        "month": 1,
        "year": 2010
      },
      "id": 734997919,
      "isCurrent": false,
      "location": {
        "name": "Wellington, New Zealand"
      },
      "startDate": {
        "month": 7,
        "year": 2008
      },
      "description": [
        "Designed scripts in Perl and C for automated processing of data"
      ],
      "title": "Software Developer",
      "technologies": [ "Perl", "C", "Bash", "Linux" ],
      "tags": [ "software", "engineering" ]
    },
    {
      "company": {
        "name": "Upper Hutt Community Rescue (UHCR) (NZRT8)",
        "href": "https://uhcr.org.nz"
      },
      "title": "Senior Operational Team Member",
      "startDate": {
        "year": 2008,
        "month": 8
      },
      "endDate": {
        "year": 2014,
        "month": 4
      },
      "description": [
        "Swiftwater rescue technician",
        "Rope responder",
        "First aider",
        "4WD vehicle operator",
        "Technology expert"
      ],
      "tags": [ "rescue", "safety" ]
    },
    {
      "company": {
        "name": "Wellington Regional Emergency Management Office",
        "href": "http://www.wremo.nz/"
      },
      "title": "Civil Defence Volunteer",
      "startDate": {
        "year": 2008,
        "month": 6
      },
      "endDate": {
        "year": 2014,
        "month": 04
      },
      "tags": [ "rescue", "safety" ]
    },
    {
      "company": {
        "name": "Solarhomes"
      },
      "className": "noprint",
      "endDate": {
        "month": 6,
        "year": 2008
      },
      "id": 712630753,
      "isCurrent": false,
      "location": {
        "name": "Wellington, New Zealand"
      },
      "startDate": {
        "month": 12,
        "year": 2007
      },
      "description": [
        "Designed automated home ventilation systems",
        "Reverse-engineered and programmed Microchip PIC based automation controllers, and programmed Omron PLC controllers"
      ],
      "title": "Design Engineer",
      "technologies": [ "Omron PLCs", "C", "PIC microcontrollers" ],
      "tags": [ "software", "hardware", "engineering" ]
    },
    {
      "company": {
        "name": "Ropes and Technical Support (RATS) (NZRT1)",
        "href": "https://www.facebook.com/nzrt1"
      },
      "title": "Team Member",
      "startDate": {
        "year": 2002,
        "month": 11
      },
      "endDate": {
        "year": 2005,
        "month": 10
      },
      "description": [
        "NZ USAR rope responder",
        "NZ USAR general rescue",
        "USAR search dog dogsbody"
      ],
      "tags": [ "rescue", "safety" ]
    },
    {
      "company": {
        "name": "Civil Defence Emergency Management Canterbury",
        "href": "http://www.cdemcanterbury.govt.nz/"
      },
      "title": "Civil Defence Volunteer",
      "startDate": {
        "year": 2002,
        "month": 2
      },
      "endDate": {
        "year": 2005,
        "month": 10
      },
      "tags": [ "rescue", "safety" ]
    },
    {
      "company": {
        "name": "Freelance",
      },
      "endDate": {
        "year": 2008
      },
      "id": 380174126,
      "isCurrent": false,
      "location": {
        "name": "Wellington, New Zealand"
      },
      "startDate": {
        "year": 2004
      },
      "description": [
        "Designed and developed web applications using HTML, CSS, JavaScript, PHP, MySQL and ActionScript for multiple clients",
        "Interacted with customers during entire development life cycle"
      ],
      "title": "Web Application Engineer",
      "technologies": [ "HTML", "JavaScript", "CSS", "PHP", "Wordpress", "MySQL",
          "ActionScript"],
      "tags": [ "web", "software", "engineering" ]
    },
    {
      "company": {
        "id": 77529,
        "industry": "Telecommunications",
        "name": "Tait Communications",
        "size": "501-1000",
        "type": "Privately Held",
        "href": "https://www.taitradio.com/"
      },
      "endDate": {
        "month": 2,
        "year": 2007
      },
      "id": 712632746,
      "isCurrent": false,
      "location": {
        "name": "Christchurch, New Zealand"
      },
      "startDate": {
        "month": 11,
        "year": 2006
      },
      "description": [
        "Developed a radio log analyser and web-interface using Python and SQLite to detect and investigate inconsistencies in radio products"
      ],
      "title": "Junior Design Engineer (Summer Internship)",
      "technologies": [ "Python", "JavaScript", "HTML", "JavaScript", "CSS", "SQLite" ],
      "tags": [ "web", "software", "engineering" ]
    },
    {
      "company": {
        "name": "BrowserCRM"
      },
      "endDate": {
        "month": 2,
        "year": 2006
      },
      "id": 712635239,
      "isCurrent": false,
      "location": {
        "name": "Wellington, New Zealand"
      },
      "startDate": {
        "month": 11,
        "year": 2005
      },
      "description": [
        "Developed client-side (JavaScript, CSS and HTML) and server-side (PHP and MySQL) parts of the BrowserCRM interface"
      ],
      "title": "Developer",
      "technologies": [ "JavaScript", "HTML", "CSS", "PHP", "MySQL" ],
      "tags": [ "web", "software", "engineering" ]
    }
  ],
  "education": [
    {
      "school": {
        "name": "Nelson Marlborough Institute of Technology",
        "href": "https://www.nmit.co.nz"
      },
      "course": {
        "name": "PRT301 Predator Trapping Methods",
        "href": "https://www.nmit.ac.nz/study/short-courses/prt301-predator-trapping-methods/"
      },
      "date": {
        "month": 10,
        "year": 2021
      },
      "tags": [ "conservation" ]
    },
    {
      "school": {
        "name": "NAUI",
        "href": "https://www.naui.org/"
      },
      "course": {
        "name": "Instructor Training Course",
        "href": "https://www.naui.org/certifications/leadership/instructor/"
      },
      "startDate": {
        "month": 02,
        "year": 2021
      },
      "endDate": {
        "month": 05,
        "year": 2021
      },
      "tags": [ "ocean", "safety" ]
    },
    {
      "school": {
        "name": "Department of Conservation (DOC)",
        "href": "https://www.doc.govt.nz"
      },
      "course": [
        {
          "name": "Introduction to Natural Heritage Online Course",
          "href": "https://www.doc.govt.nz/get-involved/training/online-courses/introduction-to-natural-heritage-online-course/"
        },
        {
          "name": "Field Skills Online Course",
          "href": "https://www.doc.govt.nz/get-involved/training/online-courses/field-skills-online-course/"
        }
      ],
      "date": {
        "month": 10,
        "year": 2020
      },
      "tags": [ "conservation" ]
    },
    {
      "school": {
        "name": "National Association of Underwater Instructors (NAUI)",
        "href": "https://www.naui.org"
      },
      "course": {
        "name": "First Aid for Dive Professionals"
      },
      "date": {
        "month": 06,
        "year": 2021
      },
      "tags": [ "ocean", "safety", "conservation" ]
    },
    {
      "school": {
        "name": "The Knowledge Academy",
        "href": "https://www.theknowledgeacademy.com/"
      },
      "course": {
        "name": "Scrum Master Certification",
        "href": "https://www.theknowledgeacademy.com/courses/scrum-training/scrum-master-training/"
      },
      "date": {
        "month": 06,
        "year": 2019
      },
      "tags": [ "web", "engineering" ]
    },
    {
      "school": {
        "name": "Purple Turtle Diving",
        "href": "http://www.purpleturtlediving.com"
      },
      "course": {
        "name": "PADI Emergency First Response",
        "href": "https://www.padi.com/courses/efr"
      },
      "date": {
        "month": 07,
        "year": 2019
      },
      "description": [
        "Primary Care (CPR) & Sec Care (1st aid)",
        "CPR/First Aid/Care for Children",
        "Emergency Oxygen Provider"
      ],
      "tags": [ "rescue", "safety" ]
    },
    {
      "school": {
        "name": "Fire Aid",
        "href": "https://fireaid.com"
      },
      "course": [
        {
          "name": "Certificate of Proficiency in Personal Survival Techniques",
          "href": "https://fireaid.com/academy-items/personal-survival-techniques/"
        }
      ],
      "date": {
        "month": 2,
        "year": 2019
      },
      "tags": [ "ocean", "safety" ]
    },
    {
      "school": {
        "name": "Plataforma Oceanica de Canarias",
        "href": "https://www.plocan.eu/en/home/"
      },
      "course": [
        {
          "name": "PLOCAN Glider School Diploma",
          "href": "http://www.gliderschool.eu/"
        }
      ],
      "date": {
        "month": 9,
        "year": 2018
      },
      "description": {
        "elements": [
          "Covering:",
          [
            "Technical overview of autonomous marine vehicles (AMVs) including Seagliders, Slocums, SeaExplorers and Wave Gilders",
            "Basic maintenance and ballasting of Slocum gliders",
            "Deployment and piloting of the AMVs mentioned above"
          ]
        ]
      },
      "tags": [ "ocean", "engineering" ],
      "className": "noprint",
    },
    {
      "school": {
        "name": "Plas Menai",
        "href": "https://www.plasmenai.co.uk/"
      },
      "course": [
        {
          "name": "Rescue 3 Swiftwater and Flood Rescue Boat Operator (SFRBO)",
          "href": "https://www.rescue3europe.com/index.php/swiftwater-and-flood-rescue-boat-operator"
        }
      ],
      "date": {
        "month": 11,
        "year": 2016
      },
      "tags": [ "rescue", "safety" ]
    },
    {
      "school": {
        "name": "Plas Menai",
        "href": "https://www.plasmenai.co.uk/"
      },
      "course": [
        {
          "name": "Rescue 3 Swiftwater and Flood Rescue Technician (SRT)",
          "href": "https://www.rescue3europe.com/index.php/swiftwater-and-flood-rescue-technician"
        }
      ],
      "date": {
        "month": 9,
        "year": 2016
      },
      "tags": [ "rescue", "safety" ]
    },
    {
      "school": {
        "name": "New Forest Navigation",
        "href": "https://newforestnavigation.co.uk"
      },
      "course": [
        {
          "name": "Map Reading and Navigation (Two-day)",
          "href": "https://newforestnavigation.co.uk/navigation-courses/one-and-two-day/"
        }
      ],
      "date": {
        "month": 3,
        "year": 2017
      },
      "tags": [ "safety", "rescue" ]
    },
    {
      "school": {
        "name": "MongoDB University",
        "href": "https://university.mongodb.com"
      },
      "course": [
        {
          "name": "MongoDB for Node.js Developers",
          "code": "M101JS",
          "href": "https://university.mongodb.com/courses/M101JS/about"
        },
        {
          "name": "MongoDB for DBAs",
          "code": "M102",
          "href": "https://university.mongodb.com/courses/M102/about"
        }
      ],
      "date": {
        "month": 2,
        "year": 2015
      },
      "tags": [ "web", "engineering", "software" ]
    },
    {
      "className": "noprint",
      "school": {
        "name": "Wellington Institute of Technology",
        "href": "http://www.weltec.ac.nz"
      },
      "location": {
        "name": "Wellington, NZ",
        "city": "Wellington",
        "country": "nz"
      },
      "date": {
        "month": 4,
        "year": 2014
      },
      "course": {
        "name": "Electrical Trade Revision Course"
      },
      "certification": {
        "name": "Electrical Workers Registration Board (EWRB) Electrical Engineer Registration",
        "href": "https://www.ewrb.govt.nz/for-registered-electrical-workers/limits-of-work/#electrical-engineer-no-limitations"
      },
      "tags": [ "engineering" ]
    },
    {
      "school": {
        "name": "NZ Safety"
      },
      "course": [
        {
          "name": "Fall Arrest Systems",
          "href": "https://www.sitesafe.org.nz/training/our-training-courses/fallarrest/"
        }
      ],
      "date": {
        "month": 5,
        "year": 2013
      },
      "location": {
        "name": "Wellington, NZ"
      },
      "tags": [ "rescue", "safety" ]
    },
    {
      "school": {
        "name": "Training Ventures",
        "href": "https://trainingventures.co.nz"
      },
      "course": [
        {
          "name": "Farm Skills - Chainsaw Operations"
        },
        {
          "name": "Farm Skills - Tree Felling"
        },
        {
          "name": "Farm Skills - Quad Bike"
        },
        {
          "name": "Farm Skills - ATV & 4WD"
        }
      ],
      "date": {
        "year": 2011
      },
      "location": {
        "name": "Wellington, NZ"
      },
      "tags": [ "rescue", "safety" ]
    },
    {
      "school": {
        "name": "Framework Solutions Ltd"
      },
      "course": [
        {
          "name": "NZQA Assessors Course"
        }
      ],
      "date": {
        "month": 8,
        "year": 2010
      },
      "tags": [ "rescue", "safety" ]
    },
    {
      "school": {
        "name": "Wellington Free Ambulance"
      },
      "course": [
        {
          "name": "Pre-Hospital Emergency Care (PHEC)"
        }
      ],
      "date": {
        "month": 8,
        "year": 2009
      },
      "tags": [ "rescue", "safety" ]
    },
    {
      "school": {
        "name": "Auldhouse",
        "href": "http://www.auldhouse.co.nz"
      },
      "location": {
        "name": "Wellington, NZ"
      },
      "date": {
        "month": 11,
        "year": 2009
      },
      "certification": {
        "name": "Redhat Enterprise Linux (RHEL) Certified Engineer"
      },
      "tags": [ "web", "engineering" ]
    },
    {
      "school": {
        "name": "Christchurch Civil Defence"
      },
      "course": [
        {
          "name": "Coordinated Incident Management System, Level 2"
        },
        {
          "name": "Use of Two-way Radios"
        },
        {
          "name": "Operating a Civil Defence Center"
        },
        {
          "name": "General Urban Search and Rescue (Orange Card)"
        },
        {
          "name": "Rope Rescue Responder"
        }
      ],
      "startDate": {
        "year": 2006
      },
      "endDate": {
        "year": 2007
      },
      "tags": [ "rescue", "safety" ]
    },
    {
      "school": {
        "name": "University of Canterbury",
        "href": "http://www.canterbury.ac.nz"
      },
      "location": {
        "name": "Christchurch, NZ",
        "city": "Christchurch",
        "country": "nz"
      },
      "certification": {
        "name": "Bachelor of Engineering with Honours",
        "specialisation": "Electrical and Electronic Engineering"
      },
      "startDate": {
        "month": 2,
        "year": 2004
      },
      "endDate": {
        "month": 11,
        "year": 2007
      },
      "description": {
        "elements": [
          "Specialising in:",
          [
            "Computer Hardware",
            "Computer Software",
            "Communications",
            "Electronic Materials & Device Engineering",
            "Engineering Management"
          ]
        ]
      },
      "tags": [ "web", "engineering", "software", "hardware" ]
    },
    {
      "className": "noprint",
      "school": {
        "name": "Onslow College",
        "href": "http://www.onslow.school.nz"
      },
      "location": {
        "name": "Wellington, NZ",
        "city": "Wellington",
        "country": "nz"
      },
      "certification": [
        {
          "name": "Bursary",
          "grade": "A Bursary",
          "href": "http://www.nzqa.govt.nz/qualifications-standards/results-2/secondary-school-qualifications-prior-to-2002/"
        },
        {
          "name": "Sixth Form Certificate",
          "grade": "University Entrance",
          "href": "http://www.nzqa.govt.nz/qualifications-standards/results-2/secondary-school-qualifications-prior-to-2002/"
        },
        {
          "name": "Electronics Technology Level 2",
        }
      ],
      "startDate": {
        "month": 2,
        "year": 1999
      },
      "endDate": {
        "month": 11,
        "year": 2003
      }
    }
  ],
  "memberships": [
    {
      "organisation": {
        "name": "Institute of Electrical and Electronic Engineers (IEEE)",
        "href": "http://www.ieee.org"
      },
      "startDate": {
        "year": 2004
      },
      "description": [
        "Ocean Engineering Society (2019 - now)"
      ],
      "tags": [ "engineering", "software", "hardware", "web", "ocean" ]
    },
  ],
  "interests": [
    'Technology',
    'Computers',
    {
      "name": "Podcasts",
      "description": [
        {
          "name": "Mongabay Podcast",
          "href": "https://www.mongabay.com/podcast/"
        },
        {
          "name": "Nature Podcast",
          "href": "https://www.nature.com/nature/podcast/archive.html"
        },
        {
          "name": "Syntax",
          "href": "https://syntax.fm/"
        },
        {
          "name": "JavaScript Jabber",
          "href": "https://devchat.tv/show/javascript-jabber/"
        },
        {
          "name": "Digital Planet",
          "href": "https://www.bbc.co.uk/programmes/p002w6r2"
        },
        {
          "name": "RNZ: Our Changing World",
          "href": "https://www.rnz.co.nz/national/programmes/ourchangingworld"
        },
        {
          "name": "DOC Sounds of Science",
          "href": "https://www.doc.govt.nz/podcast"
        },
        {
          "name": "Risky Biz",
          "href": "https://www.risky.biz",
        },
        {
          "name": "Quirks and Quarks",
          "href": "http://cbc.ca/quirks"
        },
        {
          "name": "Living on Earth",
          "href": "http://loe.org"
        }
      ]
    },
    "Hiking",
    "Scuba Diving",
    "The Outdoors",
    {
      "name": "Volunteering",
      "description": [
        {
          "name": "Search and rescue",
          "description": [
            {
              "name": "Upper Hutt Community Rescue",
              "href": "https://uhcr.org.nz/"
            }
          ]
        },
        {
          "name": "Environment conservation projects",
          "description": [
            {
              "name": "Conservation Volunteers NZ",
              "href": "https://conservationvolunteers.co.nz/"
            }
          ]
        }
      ]
    }
  ],
  "references": {
    "javascript": "https://developer.mozilla.org/en-US/docs/Web/javascript",
    "html": "https://developer.mozilla.org/en-US/docs/Web/HTML",
    "css": "https://developer.mozilla.org/en-US/docs/Web/CSS",
    "node.js": "https://nodejs.org/",
    "svelte": "https://svelte.dev/",
    "japa": "https://github.com/thetutlage/japa",
    "webdriverio": "https://webdriver.io/",
    "git": "https://git-scm.com/",
    "vim": "http://www.vim.org/",
    "uplot": "https://github.com/leeoniya/uPlot",
    "leaflet.js": "https://leafletjs.com/",
    "qgis": "https://www.qgis.org/en/site/",
    "kicad": "http://www.kicad.org/",
    "python": "https://python.org/",
    "nuxt": "https://nuxtjs.org/",
    "postgresql": "https://www.postgresql.org/",
    "plotly.js": "https://plotly.com/javascript/",
    "gitlab": "https://gitlab.com",
    "react": "https://reactjs.org/",
    "redux": "https://redux.js.org/",
    "mongodb": "https://www.mongodb.com/",
    "c": "https://wikipedia.org/wiki/C_(programming_language)",
    "amazon_lambda": "https://aws.amazon.com/lambda/",
    "amazon_sns": "https://aws.amazon.com/sns/",
    "jenkins": "https://www.jenkins.io/",
    "angular": "https://angular.io/",
    "angularjs": "https://angularjs.org/",
    "mocha": "https://mochajs.org/",
    "bootstrap": "https://getbootstrap.com/",
    "django": "http://www.djangoproject.com/",
    "jsonrpc": "https://www.jsonrpc.org/",
    "chartjs": "https://www.chartjs.org/",
    "jira": "https://www.atlassian.com/software/jira",
    "orbeon_forms": "https://www.orbeon.com/",
    "xforms": "http://wikipedia.org/wiki/XForms",
    "xslt": "http://en.wikipedia.org/wiki/XSLT",
    "xml": "https://developer.mozilla.org/en-US/docs/Web/XML/XML_introduction",
    "tfs": "https://visualstudio.com/tfs",
    "gulp": "https://gulpjs.com/",
    "coldfusion": "https://coldfusion.adobe.com/",
    "wordpress": "https://wordpress.org/",
    "php": "https://www.php.net/",
    "drupal": "https://www.drupal.org/",
    "perl": "https://www.perl.org/",
    "vmware": "https://www.vmware.com/",
    "puppet": "https://puppet.com/",
    "sqlite": "https://sqlite.org/index.html",
    "mysql": "https://www.mysql.com/",
    "preact": "https://preactjs.com/",
    "pic_microcontrollers": "https://www.microchip.com/en-us/products/microcontrollers-and-microprocessors/8-bit-mcus/pic-mcus",
    "aws_s3": "https://aws.amazon.com/s3/",
    "c#": "https://en.wikipedia.org/wiki/C_Sharp_(programming_language)",
    "nb-iot": "https://en.wikipedia.org/wiki/Narrowband_IoT",
    "lte-m": "https://en.wikipedia.org/wiki/LTE-M",
    "mqtt": "https://mqtt.org/",
    "iridium_sbd": "https://www.iridium.com/services/iridium-sbd/"
  }
};

var main;
var search;
var sections = [];
var current;
var showTechnologies = true;

/**
 * Create an HTML element
 *
 * @param {string} type Type of element to create
 * @param {{ [attribute: string]: string | number | boolen}} [attributes] HTML
 *   element attributes
 * @param {string|HTMLElement} ...children Children HTML strings or
 *   HTMLElements to append to the create HTML element
 *
 * @returns {HTMLElement}
 */
function createElement(type) {
  if (typeof type === 'undefined') {
    type = 'p';
  } else if (typeof type !== 'string') {
    throw new Error('type must be a string');
  }

  var dom, el;

  var args = Array.from(arguments);
  args.shift();
  var length = args.length;

  var parameters;

  if (args.length && !(args[0] instanceof Array)
      && !(args[0] instanceof HTMLElement) && typeof args[0] === 'object') {
    parameters = args.shift();
    length--;
  }

  function create() {
    var element = document.createElement(type);
    if (parameters) {
      Object.keys(parameters).forEach(function (parameter) {
        switch (parameter) {
          case 'aria-pressed':
            element.setAttribute(parameter, parameters[parameter]);
            break;
          default:
            element[parameter] = parameters[parameter];
        }
      })
    }

    return element;
  }

  if (!length) {
    return create();
  } else if (length === 1) {
    if (args[0] instanceof Array) {
      args = args[0];
      length = args.length;
    } else if (args[0] instanceof HTMLElement) {
      dom = create(),
      dom.appendChild(args[0]);
      return dom;
    } else {
      dom = create(),
      dom.innerHTML = args[0];
      return dom;
    }
  }

  dom = [];
  for (i = 0; i < length; i++) {
    if (typeof args[i] === 'undefined' || args[i] === null) {
    } else if (args[i] instanceof HTMLElement) {
      dom.push((el = create()));
      el.appendChild(args[i]);
    } else if (typeof args[i] === 'object') {
    } else {
      dom.push((el = create()));
      el.innerHTML = args[i];
    }
  }

  return dom;
}

/**
 * Append element(s) to another element
 *
 * @param {HTMLElement} dom Element to append element(s) to
 * @param {HTMLElement} ...children Elements to append to the given element
 */
function append(dom) {
  var args = Array.from(arguments);
  args.shift();
  var child;

  for (a in args) {
    child = args[a];
    if (typeof child === 'undefined' || child === null) {
      continue;
    }
    if (child instanceof Array) {
      var i;
      for (i in child) {
        dom.appendChild(child[i]);
      }
    } else if (child instanceof Node) {
      dom.appendChild(child);
    } else {
      dom.appendChild(document.createTextNode(child));
    }
  }

  return dom;
}

/**
 * Create a new resume section
 *
 * @param {string} Section title
 *
 * @returns {HTMLElement} New section HTMLElement
 */
function section(title, classes) {
  var el = createElement('section',
      createElement('h2', title));
  el.className = title.toLowerCase().replace(' ', '_');
  return el;
}

/**
 * @typedef {{className: string, [key: string]: any }} ArticleObject Article
 *   data object
 */
/**
 * @typedef Renderer (object: ArticleObject, article: HTMLElement) => void
 */

/**
 * Create a article element, the content rendered with the given renderer
 *
 * @param {ArticleObject} object Article data object
 * @param {Renderer} renderer
 *   Renderer function that will render the contents of the article
 * @param {string} [id] ID of the article
 *
 * @returns {HTMLElement} The article element
 */
function article(object, renderer, id) {
  var dom = document.createElement('article');

  if (object.className) {
    dom.className = object.className;
  }

  if (id) {
    dom.id = id;
  }

  renderer(object, dom);

  return dom;
}

/**
 * Creates an element for a name with an optional link from the given data
 * object
 *
 * @param {object} data Data object
 * @param {string} data.name Name for element
 * @param {string} [data.href] Link for element
 * @param {string} [data.graph] Additional name for element
 *
 * @returns {HTMLElement} The link label element
 */
function linkLabel(data, type) {
  var el = document.createElement(type),
      link;

  if (data.href) {
    link = el.appendChild(document.createElement('a'));
    link.setAttribute('target', '_blank');
    link.setAttribute('href', data.href);
    link.innerHTML = data.name + (data.grade ? ' (' + data.grade + ')': '');
  } else {
    el.innerHTML = data.name + (data.grade ? ' (' + data.grade + ')': '');
  }

  return el;
}

/**
 * Create a list of elements for the given list, optionally creating links
 * for each element using the resume references
 *
 * @param {string[]} list List of items
 * @param {string} tag Tag to use for list elements
 * @param {boolean} [link] Whether or not to try and create a link for each
 *   list element
 */
function makeList(list, tag, link) {
  var string = '',
      start = (tag ? '<' + tag + '>' : ''),
      end = (tag ? '</' + tag + '>' : '');

  if (list instanceof Array) {
    if (list.length) {
      if (link) {
        for (var i = 0; i < list.length; i++) {
          var slug = list[i].toLowerCase().replace(/ /g, '_');

          if (resume.references[slug]) {
            string += start + '<a href="' + resume.references[slug]
                + '" target="_blank">' + list[i] + '</a>' + end + ' ';
          } else {
            string += start + list[i] + end + ' ';
          }
        }
      } else {
        string = start + list.join(end + start + ' ') + end;
      }
    } else {
      string = '';
    }
  }

  return string;
}

/**
 * Create location element
 *
 * @param {object} data Location data
 * @param {string} data.name Location name
 * @param {string} data.className Location classes
 *
 * @returns {HTMLElement} Location element
 */
function loc(data) {
  if (data.name) {
    el = createElement('span', data.name);
  } else {
  }
  el.className = 'location';
  return el;
}

var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

/**
 * @typedef {object} DateObject Date object
 * @property {number} month Month
 * @property {number} year Year
 */

/**
 * Create a data HTML element
 *
 * @param {DateObject} date Date to create an element for
 *
 * @returns {HTMLElement} Date element
 */
function makeDate(date) {
  var dom = createElement('date');
  if (typeof date === 'string') {
    append(dom, date);
  } else {
    append(dom,
        (date.month ? months[date.month-1] : undefined),
        ' ', date.year);
  }
  return dom;
}

/**
 * Create elements containing a date or a date range
 *
 * @param {
 *   { date: DateObject; [key: string]: any } |
 *   { startDate: DateObject; endDate: DateObject; [key: string]: any }
 * } data Date data
 *
 * @returns {HTMLElement} Element containing the date or date range
 */
function date(data) {
  var dom;

  if (data.date) {
    return makeDate(data.date);
  } else if (data.startDate || data.endDate) {
    dom = createElement('p');

    if (data.startDate && data.endDate) {
      append(dom, makeDate(data.startDate), ' - ', makeDate(data.endDate));
    } else if (data.startDate) {
      append(dom, makeDate(data.startDate), ' - now');
    } else if (data.endDate) {
      return makeDate(data.endDate);
    }

    dom.className = 'date';
    return dom;
  }
}

/**
 * Create descriptions elements for the given data
 *
 * @param {Description} Description data
 *
 * @returns {HTMLElement} HTML element containing the description elements
 */
function description(data) {
  var d, el, dom;
  if (data instanceof Array) {
    if (data.length) {
      dom = createElement('ul');
      for (d in data) {
        if (typeof data[d] === 'string') {
          el = createElement('li', data[d]);
          append(dom, el);
        } else if (data[d] instanceof Array) {
          append(dom, createElement('li', createElement('ul', description(data[d]))));
        } else if (data[d] instanceof Object) {
          if (data[d].name) {
            el = linkLabel(data[d], 'li')
            append(dom, el);
            if (data[d].description) {
              append(el, description(data[d].description));
            }
          } else {
            var attributes = Object.assign({}, data[d]);
            delete attributes.description;

            append(dom, createElement('li', attributes, data[d].description));
          }
        }
      }
    }
  } else if (data instanceof Object) {
    if (data.elements) {
      // If data.type is set, create a div and then an type element for each element
      if (data.type) {
        dom = createElement('div');
      } else {
        dom = createElement('p');
      }
      for (d in data.elements) {
        if (typeof data.elements[d] === 'undefined') {
        } else if (typeof data.elements[d] === 'object') {
          append(dom, description(data.elements[d]));
        } else {
          if (data.type) {
            append(dom, createElement(data.type, data.elements[d]));
          } else {
            append(dom, document.createTextNode(data.elements[d]));
          }
        }
      }
    }
  } else if (typeof data === 'undefined') {
    return;
  } else {
    return createElement('p', data);
  }

  return dom;
}

/**
 * Create a list of articles from the given data
 *
 * @param {object[]} data Article object array
 * @param {Renderer} Article renderer
 * @param {string} id ID of articles
 *
 * @returns {HTMLElement[]} Array of article elements
 */
function list(data, renderer, id) {
  var d, doms = [];

  for (d in data) {
    doms.push(article(data[d], renderer, id ? id + '-' + d : null));
  }

  return doms;
}

/**
 * Renderer for the experiences section
 *
 * @param data Experiences data
 * @param {HTMLElement} obj Section HTML element
 */
function experience(data, obj) {
  var el, link;

  append(obj, date(data));

  if (data.title) {
    obj.appendChild(document.createElement('h3')).innerHTML = data.title;
  }

  if (data.company) {
    el = obj.appendChild(linkLabel(data.company, 'p'));
    el.className = 'company';
  }

  if (data.location) {
    link = el.appendChild(loc(data.location));
  }

  if (data.description) {
    append(obj, description(data.description))
  }

  if (data.technologies) {
    var technologies;
    if (data.technologies instanceof Array) {
      technologies = makeList(data.technologies, 'i', true);
    } else {
      technologies = data.technologies;
    }

    if (showTechnologies) {
      el = createElement('p', 'Technologies used: ' + technologies);
      el.className = 'technologies';
      append(obj, el);
    }
  }
}

/**
 * Renderer for the development section
 *
 * @param data Development data
 * @param {HTMLElement} obj Section HTML element
 */
function course(data, obj) {
  var el, link, d, courseType = 'h3';

  append(obj, date(data));

  if (data.certification) {
    if (data.certification instanceof Array) {
      for (d in data.certification) {
        obj.appendChild(linkLabel(data.certification[d], 'h3'));
        }
    } else {
      obj.appendChild(linkLabel(data.certification, 'h3'));
    }
    courseType = 'h4';
  }

  if (data.course) {
    if (data.course instanceof Array) {
      for (d in data.course) {
        obj.appendChild(linkLabel(data.course[d], courseType));
        }
    } else {
      obj.appendChild(linkLabel(data.course, courseType));
    }
  }

  if (data.school) {
    if (data.school instanceof Array) {
      el = obj.appendChild(createElement('p'));
      for (d in data.school) {
        el.appendChild(linkLabel(data.school[d], 'span'));
        }
    } else {
      el =obj.appendChild(linkLabel(data.school, 'p'));
    }
    el.className = 'school';
  }

  if (data.location) {
    link = el.appendChild(loc(data.location));
  }

  el = obj.appendChild(document.createElement('div'));
  el.className = 'dateRange';
  append(obj, description(data.description))
}

/**
 * Renderer for the memberships section
 *
 * @param data Memberships data
 * @param {HTMLElement} obj Section HTML element
 */
function memberships(data, obj) {
  var el, link;

  append(obj, date(data));

  obj.appendChild(linkLabel(data.organisation, 'h3'));

  if (data.memberType) {
    el = obj.appendChild(linkLabel(data.memberType, 'p'));
    el.className = 'memberType';
  }

  append(obj, description(data.description))
}

search = document.querySelector('#search');
main = document.querySelector('body > main');
current = main;
tags = createElement('div', { className: 'tags buttons' });

append(main,
    append(tags, createElement('strong', {}, 'Tags: ')),
    append(
      section('Personal Profile'),
      createElement('p', resume.profile || resume.summary.join(' ')),
      showTechnologies && resume.favouredTech ?
          createElement('p', { className: 'technologies' }, 'Favoured technologies: ' + makeList(resume.favouredTech, 'i', true)) : null
    ),
    append(section('Experiences'), list(resume.experiences, experience, 'experiences')),
    append(section('Development'), list(resume.education, course, 'education')),
    append(section('Memberships'), list(resume.memberships, memberships, 'memberships')),
    append(section('Interests'), description(resume.interests)));

// Enable tags
allTags = [];

tagSections = ['experiences', 'education'];

for (x = 0; x < tagSections.length; x++) {
  section = resume[tagSections[x]];
  for (y = 0; y < section.length; y++) {
    if (Array.isArray(section[y].tags)) {
      for (z = 0; z < section[y].tags.length; z++) {
        if (allTags.indexOf(section[y].tags[z].toLowerCase()) === -1) {
          allTags.push(section[y].tags[z].toLowerCase());
        }
      }
    }
  }
}

allTags.sort();

var urlParams = new URLSearchParams(window.location.search);
var activeTags = urlParams.getAll('tag');
var extraClasses = urlParams.getAll('class');
var searchTerm = urlParams.get('search');
if (extraClasses.length) {
  document.body.classList.add(...extraClasses);
}
if (!activeTags.length) {
  activeTags = [  ];
}
if (searchTerm && !search.value) {
  search.value = searchTerm;
}

var tagHideClass = 'tagHidden';
function toggleTags(tag, event) {
  if (typeof tag === 'string') {
    index = activeTags.indexOf(tag);
    if (index === -1) {
      activeTags.push(tag);
      if (event) {
        event.target.setAttribute('aria-pressed', true);
      }
    } else {
      activeTags.splice(index, 1);
      if (event) {
        event.target.setAttribute('aria-pressed', false);
      }
    }
  } else {
  }

  // Hide articles as required
  for (var x = 0; x < tagSections.length; x++) {
    section = resume[tagSections[x]];
    for (var y = 0; y < section.length; y++) {
      if (Array.isArray(section[y].tags)) {
        var dom = document.getElementById(tagSections[x] + '-' + y);
        if (dom) {
          active = false;
          if (activeTags.length) {
            for (var z = 0; z < activeTags.length; z++) {
              if (section[y].tags.indexOf(activeTags[z]) !== -1) {
                active = true;
                break;
              }
            }
          } else {
            active = true;
          }

          if (active) {
            if (dom.classList.contains(tagHideClass)) {
              dom.classList.remove(tagHideClass);
            }
          } else {
            if (!dom.classList.contains(tagHideClass)) {
              dom.classList.add(tagHideClass);
            }
          }
        }
      }
    }
  }
}

for (x = 0; x < allTags.length; x++) {
  append(tags, createElement('button', {
    'aria-pressed': activeTags.indexOf(allTags[x]) !== -1,
    onclick: (function(tag) {
      return function(event) {
        toggleTags(tag, event);
      }
    })(allTags[x])
  }, allTags[x]));
}

toggleTags();

// Set up search
activateSearch(main, search, {
  hideNodes: ['SECTION', 'ARTICLE']
});
